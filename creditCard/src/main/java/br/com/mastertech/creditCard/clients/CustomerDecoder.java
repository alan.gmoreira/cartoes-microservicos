package br.com.mastertech.creditCard.clients;

import br.com.mastertech.creditCard.exceptions.CustomerNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.apache.http.HttpStatus;

public class CustomerDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == HttpStatus.SC_NOT_FOUND)
            return new CustomerNotFoundException();
        return errorDecoder.decode(s, response);
    }
}
