package br.com.mastertech.creditCard.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE, reason = "Customer API unavailable!")
public class CustomerNotAvailableException extends RuntimeException {
}
