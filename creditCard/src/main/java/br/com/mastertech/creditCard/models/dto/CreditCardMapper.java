package br.com.mastertech.creditCard.models.dto;

import br.com.mastertech.creditCard.models.CreditCard;
import org.springframework.stereotype.Component;

@Component
public class CreditCardMapper {

    public CreditCard toCreditCard(CreateCreditCardRequest createCreditCardRequest) {
        CreditCard creditCard = new CreditCard();
        creditCard.setCustomerId(createCreditCardRequest.getCustomerId());
        creditCard.setNumber(createCreditCardRequest.getNumber());

        return creditCard;
    }

    public CreateCreditCardResponse toCreateCreditCardResponse(CreditCard creditCard)
    {
        CreateCreditCardResponse createCreditCardResponse = new CreateCreditCardResponse();
        createCreditCardResponse.setId(creditCard.getId());
        createCreditCardResponse.setCustomerId(creditCard.getCustomerId());
        createCreditCardResponse.setNumber(creditCard.getNumber());
        createCreditCardResponse.setActive(creditCard.isAtivo());

        return createCreditCardResponse;
    }

    public GetCreditCardResponse toGetCreditCardResponse(CreditCard creditCard)
    {
        GetCreditCardResponse getCreditCardResponse = new GetCreditCardResponse();
        getCreditCardResponse.setId(creditCard.getId());
        getCreditCardResponse.setCustomerId(creditCard.getCustomerId());
        getCreditCardResponse.setNumber(creditCard.getNumber());

        return getCreditCardResponse;
    }

}
