package br.com.mastertech.creditCard.services;

import br.com.mastertech.creditCard.clients.Customer;
import br.com.mastertech.creditCard.clients.CustomerClient;
import br.com.mastertech.creditCard.exceptions.CreditCardAlreadyExistsException;
import br.com.mastertech.creditCard.exceptions.CreditCardNotFoundException;
import br.com.mastertech.creditCard.models.CreditCard;
import br.com.mastertech.creditCard.models.dto.CreateCreditCardRequest;
import br.com.mastertech.creditCard.models.dto.CreateCreditCardResponse;
import br.com.mastertech.creditCard.repositories.CreditCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CreditCardService {

    @Autowired
    private CreditCardRepository creditCardRepository;

    @Autowired
    private CustomerClient customerClient;

    public CreditCard CreateCreditCard(CreditCard creditCard) {
        Customer customer = customerClient.getById(creditCard.getCustomerId());

        if(existsByNumber(creditCard.getNumber()))
            throw new CreditCardAlreadyExistsException();

        creditCard.setId(0);
        creditCard.setAtivo(false);
        creditCard.setCustomerId(customer.getId());

        return creditCardRepository.save(creditCard);
    }

    public CreditCard activateCreditCard(String number, boolean activate) {
        CreditCard creditCard = getByNumber(number);
        creditCard.setAtivo(activate);

        return creditCardRepository.save(creditCard);
    }

    public CreditCard getByNumber(String number) {
        CreditCard creditCard = creditCardRepository.findByNumber(number);
        if (creditCard != null)
            return creditCard;
        else throw new  CreditCardNotFoundException();
    }

    public CreditCard getById(int id)
    {
        Optional<CreditCard> cartao = creditCardRepository.findById(id);

        if(cartao.isPresent())
            return cartao.get();
        else throw new CreditCardNotFoundException();
    }

    private boolean existsByNumber(String number)
    {
        return creditCardRepository.existsByNumber(number);
    }

}
