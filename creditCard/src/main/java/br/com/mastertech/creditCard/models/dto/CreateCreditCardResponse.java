package br.com.mastertech.creditCard.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateCreditCardResponse {

    private int id;

    @JsonProperty("numero")
    private String number;

    @JsonProperty("clienteId")
    private int customerId;

    @JsonProperty("ativo")
    private boolean active;

    public CreateCreditCardResponse(int id, String number, int customerId, boolean active) {
        this.id = id;
        this.number = number;
        this.customerId = customerId;
        this.active = active;
    }

    public CreateCreditCardResponse() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}
