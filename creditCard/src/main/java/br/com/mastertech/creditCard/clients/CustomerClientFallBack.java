package br.com.mastertech.creditCard.clients;

import br.com.mastertech.creditCard.exceptions.CustomerNotAvailableException;

public class CustomerClientFallBack implements CustomerClient{

    @Override
    public Customer getById(int clientId) {
        throw new CustomerNotAvailableException();
    }
}
