package br.com.mastertech.creditCard.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateCreditCardRequest {

    @JsonProperty("numero")
    private String number;
    @JsonProperty("clienteId")
    private int customerId;

    public CreateCreditCardRequest() {
    }

    public CreateCreditCardRequest(String number, int customerId) {
        this.number = number;
        this.customerId = customerId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
}
