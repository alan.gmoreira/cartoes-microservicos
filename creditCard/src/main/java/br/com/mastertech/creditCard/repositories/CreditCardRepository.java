package br.com.mastertech.creditCard.repositories;

import br.com.mastertech.creditCard.models.CreditCard;
import org.springframework.data.repository.CrudRepository;

public interface CreditCardRepository extends CrudRepository<CreditCard, Integer> {

    public CreditCard findByNumber(String numero);

    public boolean existsByNumber(String numero);
}
