package br.com.mastertech.creditCard.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetCreditCardResponse {


    private int id;

    @JsonProperty("numero")
    private String number;

    @JsonProperty("clienteId")
    private int customerId;

    public GetCreditCardResponse(int id, String number, int customerId) {
        this.id = id;
        this.number = number;
        this.customerId = customerId;
    }

    public GetCreditCardResponse() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

}
