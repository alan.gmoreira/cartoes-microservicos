package br.com.mastertech.creditCard.clients;

import br.com.mastertech.creditCard.exceptions.CustomerNotAvailableException;
import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class CustomerConfiguration {
    @Bean
    public ErrorDecoder getCustomerDecoder() {
        return new CustomerDecoder();
    }

    @Bean
    public Feign.Builder builder()
    {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new CustomerClientFallBack(), RetryableException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }
}
