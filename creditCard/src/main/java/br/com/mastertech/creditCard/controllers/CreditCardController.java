package br.com.mastertech.creditCard.controllers;

import br.com.mastertech.creditCard.models.CreditCard;
import br.com.mastertech.creditCard.models.dto.*;
import br.com.mastertech.creditCard.services.CreditCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;


@RestController
@RequestMapping("/cartao")
public class CreditCardController {

    @Autowired
    private CreditCardService creditCardService;

    @Autowired
    private CreditCardMapper creditCardMapper;

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public CreateCreditCardResponse createCreditCard(@RequestBody @Valid CreateCreditCardRequest createCreditCardRequest) {
        CreditCard creditCard = creditCardService.CreateCreditCard(creditCardMapper.toCreditCard(createCreditCardRequest));
        return creditCardMapper.toCreateCreditCardResponse(creditCard);
    }

    @PatchMapping("/{numero}")
    public CreateCreditCardResponse activateDesactivateCreditCard(
            @PathVariable String numero,
            @RequestBody ActivateDesactivateCreditCardRequest activateDesactivateCreditCardRequest) {

        CreditCard creditCard = creditCardService.activateCreditCard(numero, activateDesactivateCreditCardRequest.isActive());
        return creditCardMapper.toCreateCreditCardResponse(creditCard);
    }

    @GetMapping("/{numero}")
    public GetCreditCardResponse getByNumber(@PathVariable String numero) {
        CreditCard creditCard = creditCardService.getByNumber(numero);
        return creditCardMapper.toGetCreditCardResponse(creditCard);
    }

    @GetMapping("/id/{id}")
    public CreditCard getById(@PathVariable int id) {
        CreditCard creditCard = creditCardService.getById(id);
        return creditCard;
    }
}


