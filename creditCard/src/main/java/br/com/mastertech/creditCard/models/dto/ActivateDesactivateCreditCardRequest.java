package br.com.mastertech.creditCard.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ActivateDesactivateCreditCardRequest {

    @JsonProperty("ativo")
    private boolean active;

    public ActivateDesactivateCreditCardRequest() {
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}
