package br.com.mastertech.payment.exceptions;

import com.google.inject.internal.cglib.core.$Constants;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Nenhum pagamento encontrado!")
public class PaymentNotFoundException extends RuntimeException {
}
