package br.com.mastertech.payment.clients;

import br.com.mastertech.payment.exceptions.CreditCardAlreadyExistsException;
import br.com.mastertech.payment.exceptions.CreditCardNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;

public class CreditCardDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == HttpStatus.SC_NOT_FOUND)
            return new CreditCardNotFoundException();
        else if(response.status() == HttpStatus.SC_BAD_REQUEST)
            return  new CreditCardAlreadyExistsException();
        else
            return errorDecoder.decode(s, response);
    }
}
