package br.com.mastertech.payment.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.FORBIDDEN, reason = "Cartão inativo!")
public class CreditCardInactiveException extends RuntimeException {
}
