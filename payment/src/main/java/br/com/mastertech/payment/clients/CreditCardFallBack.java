package br.com.mastertech.payment.clients;

import br.com.mastertech.payment.exceptions.CreditCardNotAvailableException;

public class CreditCardFallBack implements CreditCardClient{
    @Override
    public CreditCard getById(int id) {
        throw new CreditCardNotAvailableException();
    }
}
