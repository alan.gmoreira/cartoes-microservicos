package br.com.mastertech.payment.clients;

import br.com.mastertech.payment.exceptions.CreditCardNotAvailableException;
import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class CreditCardClientConfiguration {
    @Bean
    public ErrorDecoder getCreditCardDecoder() {
        return new CreditCardDecoder();
    }

    @Bean
    public Feign.Builder builder()
    {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new CreditCardFallBack(), RetryableException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }
}

