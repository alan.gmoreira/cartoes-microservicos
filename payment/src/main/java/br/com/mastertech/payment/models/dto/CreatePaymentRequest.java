package br.com.mastertech.payment.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class CreatePaymentRequest {

    @NotNull
    @JsonProperty("cartao_id")
    private int creditCardId;

    @NotNull
    @NotBlank
    @NotEmpty
    @Length(min = 3)
    @JsonProperty("descricao")
    private String description;

    @DecimalMin("0.1")
    @JsonProperty("valor")
    private double value;

    public CreatePaymentRequest() {
    }

    public int getCreditCardId() {
        return creditCardId;
    }

    public void setCreditCardId(int creditCardId) {
        this.creditCardId = creditCardId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
