package br.com.mastertech.payment.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreatePaymentResponse {

    private int id;
    @JsonProperty("cartaoId")
    private int creditCardId;
    @JsonProperty("descricao")
    private String description;
    @JsonProperty("valor")
    private double value;

    public CreatePaymentResponse() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCreditCardId() {
        return creditCardId;
    }

    public void setCreditCardId(int creditCardId) {
        this.creditCardId = creditCardId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
