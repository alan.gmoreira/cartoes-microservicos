package br.com.mastertech.payment.repositories;

import br.com.mastertech.payment.models.Payment;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PaymentRepository extends CrudRepository<Payment, Integer> {
    public List<Payment> findByCreditCardId(int id);
}
