package br.com.mastertech.payment.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "CREDITCARD", configuration = CreditCardClientConfiguration.class)
public interface CreditCardClient {

    @GetMapping("/cartao/id/{id}")
    CreditCard getById(@PathVariable int id);
}
