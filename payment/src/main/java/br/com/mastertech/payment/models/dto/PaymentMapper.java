package br.com.mastertech.payment.models.dto;

import br.com.mastertech.payment.models.Payment;
import org.springframework.stereotype.Component;

@Component
public class PaymentMapper {

    public Payment toPayment(CreatePaymentRequest createPaymentRequest)
    {
        Payment payment = new Payment();
        payment.setCreditCardId(createPaymentRequest.getCreditCardId());
        payment.setDescription(createPaymentRequest.getDescription());
        payment.setValue(createPaymentRequest.getValue());

        return payment;
    }

    public CreatePaymentResponse toCreatePaymentResponse(Payment payment)
    {
        CreatePaymentResponse createPaymentResponse = new CreatePaymentResponse();
        createPaymentResponse.setCreditCardId(payment.getCreditCardId());
        createPaymentResponse.setDescription(payment.getDescription());
        createPaymentResponse.setId(payment.getId());
        createPaymentResponse.setValue(payment.getValue());

        return createPaymentResponse;
    }

}
