package br.com.mastertech.payment.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE, reason = "CreditCard API unavailable!")
public class CreditCardNotAvailableException extends RuntimeException {
}
