package br.com.mastertech.payment.controllers;

import br.com.mastertech.payment.models.Payment;
import br.com.mastertech.payment.models.dto.CreatePaymentRequest;
import br.com.mastertech.payment.models.dto.CreatePaymentResponse;
import br.com.mastertech.payment.models.dto.PaymentMapper;
import br.com.mastertech.payment.services.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private PaymentMapper paymentMapper;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/pagamento")
    public CreatePaymentResponse createPayment(@RequestBody @Valid CreatePaymentRequest createPaymentRequest) {
        //try {
            Payment payment = paymentService.createPayment(paymentMapper.toPayment(createPaymentRequest));
            return paymentMapper.toCreatePaymentResponse(payment);
       /* } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }*/
    }

    @GetMapping("/pagamentos/{idCartao}")
    public List<CreatePaymentResponse> retornaPagamentos(@PathVariable int idCartao) {
        //try {
        List<Payment> payments = paymentService.getPaymentByCreditCardId(idCartao);
        List<CreatePaymentResponse> paymentsResponse = payments.stream()
                .map(e -> paymentMapper.toCreatePaymentResponse(e))
                .collect(Collectors.toList());

        return paymentsResponse;

      /*} catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }*/
    }

}
