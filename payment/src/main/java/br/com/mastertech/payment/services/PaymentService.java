package br.com.mastertech.payment.services;

import br.com.mastertech.payment.clients.CreditCard;
import br.com.mastertech.payment.clients.CreditCardClient;
import br.com.mastertech.payment.exceptions.CreditCardInactiveException;
import br.com.mastertech.payment.exceptions.PaymentNotFoundException;
import br.com.mastertech.payment.models.Payment;
import br.com.mastertech.payment.repositories.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentService {

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private CreditCardClient creditCardClient;

    public Payment createPayment(Payment payment)
    {
        CreditCard creditCard = creditCardClient.getById(payment.getCreditCardId());

        if (creditCard.isAtivo()) {
            payment.setId(0);
            return paymentRepository.save(payment);
        }
        throw new CreditCardInactiveException();
    }

    public List<Payment> getPaymentByCreditCardId(int idCartao)
    {
        List<Payment> payments = paymentRepository.findByCreditCardId(idCartao);

        if(payments.size() <= 0)
            throw new PaymentNotFoundException();

        return payments;
    }
}
