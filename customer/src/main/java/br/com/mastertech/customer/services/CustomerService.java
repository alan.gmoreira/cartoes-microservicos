package br.com.mastertech.customer.services;

import br.com.mastertech.customer.exceptions.CustomerNotFoundException;
import br.com.mastertech.customer.models.Customer;
import br.com.mastertech.customer.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public Customer cadastrarCliente(String name) {
        return customerRepository.save(new Customer(0, name));
    }

    public Customer buscaPorId(int id) {
        Optional<Customer> optionalCliente = customerRepository.findById(id);
        if (optionalCliente.isPresent())
            return optionalCliente.get();

        throw new CustomerNotFoundException();
    }

    public boolean customerExistsById(int id)
    {
        return customerRepository.existsById(id);
    }

}
