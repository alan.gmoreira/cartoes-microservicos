package br.com.mastertech.customer.repositories;

import br.com.mastertech.customer.models.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {

}
